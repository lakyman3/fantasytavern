extends RigidBody3D

var audioPlayer : AudioStreamPlayer3D

func _ready():
	audioPlayer = $Bonk
	set_process(true)

func _body_entered(body: Node) -> void:
	print("1")
	if body.is_in_group("collider_group"):  # Проверка, что столкнулись с нужной группой объектов
		print("2")
		audioPlayer.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass
