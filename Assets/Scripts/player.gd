extends CharacterBody3D


@export var SPEED = 10.0
@export var SPRINT_SPEED = 20.0
@export var JUMP_VELOCITY = 5.0
#@export var JUMP_DELTA = MAX_JUMP_VELOCITY / 60.0
@export var MOUSE_SPEED = 0.1

var collider
var object_scene = preload("res://Scenes/spot.tscn")
var camera
var is_sprinted = false
var pause = false
var jump_vel = 0.0
var ray : RayCast3D
var spawnray : RayCast3D
var xp = 0
var crouch = false
var standing_height = 2.0
var crouching_height = 1.0
var target_position = Vector3(0, 0, 0)
var mesh
var animation
var crouch_vignette
var is_debug
var debug_label
var spot_label
var delta
#Звукари
var StepSound: AudioStream
var StepPlayer: AudioStreamPlayer
var JumpSound: AudioStream
var JumpPlayer: AudioStreamPlayer
var WashSound: AudioStream
var WashPlayer: AudioStreamPlayer
var ButtonSound: AudioStream
var ButtonPlayer: AudioStreamPlayer

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
	collider = $CollisionShape3D
	camera = $Camera3D
	ray = $Camera3D/RayCast3D
	spawnray = $Camera3D/Spawnray
	mesh = $MeshInstance3D
	animation = $Camera3D/AnimationPlayer
	crouch_vignette = $Camera3D/TextureRect
	debug_label = $Camera3D/Debug_Label
	debug_label.visible = false
	spot_label = $Camera3D/Spot_label
	
	
#func run_fov():
	#if is_sprinted and velocity.length() != 0:
		#animation.play("Run")
		
	StepSound = load("res://Assets/Audio/Foot_step.wav")
	StepPlayer = AudioStreamPlayer.new()
	StepPlayer.stream = StepSound
	StepPlayer.autoplay = false
	add_child(StepPlayer)
	
	JumpSound = load("res://Assets/Audio/Jump.wav")
	JumpPlayer = AudioStreamPlayer.new()
	JumpPlayer.stream = JumpSound
	JumpPlayer.autoplay = false
	JumpPlayer.volume_db = -10
	add_child(JumpPlayer)

	WashSound = load("res://Assets/Audio/Wash.wav")
	WashPlayer = AudioStreamPlayer.new()
	WashPlayer.stream = WashSound
	WashPlayer.autoplay = false
	WashPlayer.volume_db = -10
	add_child(WashPlayer)
	
	ButtonSound = load("res://Assets/Audio/Button.wav")
	ButtonPlayer = AudioStreamPlayer.new()
	ButtonPlayer.stream = ButtonSound
	ButtonPlayer.autoplay = false
	ButtonPlayer.volume_db = -10
	add_child(ButtonPlayer)

func _input(event):
	if event is InputEventMouseMotion and Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		camera.rotate_x(deg_to_rad(event.relative.y * MOUSE_SPEED * -1))
		rotate_y(deg_to_rad(event.relative.x * MOUSE_SPEED * -1))
	if Input.is_action_just_pressed("crouch")  and is_on_floor():
		if crouch:
			uncrouching()
		else:
			crouching()

func crouching():
	collider.shape.height = crouching_height
	SPEED = 5.0
	mesh.scale.y -= 0.15
	mesh.transform.origin.y += 0.5
	crouch = true
	animation.play("Crouch")
	
func uncrouching():
	collider.shape.height = standing_height
	mesh.scale.y += 0.15
	mesh.transform.origin.y -= 0.5
	SPEED = 10.0
	crouch = false
	animation.play("Uncrouch")
	
func spawn_object(position):
	var new_object = object_scene.instantiate() # Создание экземпляра объекта
	get_tree().get_root().add_child(new_object) # Добавление объекта к родителю игрока
	new_object.global_transform.origin = position # Установка позиции объекта
	ButtonPlayer.play()
			
func _physics_process(delta):
	if Input.is_action_just_pressed("spawn"):
		if spawnray.is_colliding():
			var spawn_position = spawnray.get_collision_point()
			print(spawn_position)
			spawn_object(spawn_position)
			
	# Debug jump
	if Input.is_action_just_pressed("debug"):
		if is_debug:
			JUMP_VELOCITY = 5.0
			is_debug = false
			debug_label.visible = false
			ButtonPlayer.play()
		else:
			JUMP_VELOCITY = 60.0
			is_debug = true
			debug_label.visible = true
			ButtonPlayer.play()
			
	if is_on_floor() and get_velocity().length() > 0:
		if !StepPlayer.playing:
			StepPlayer.play()
			var speedRatio = get_velocity().length() / 100
			StepPlayer.pitch_scale = 1.0 + speedRatio
			StepPlayer.volume_db = -10.0 + speedRatio * 50
				
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
		
	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += JUMP_VELOCITY
		JumpPlayer.play()
		
		if crouch:
			uncrouching()
		
	if Input.is_action_just_pressed("return"):
		position = Vector3(0,2,0)
		velocity = Vector3.ZERO
	
	if Input.is_action_pressed("sprint") and not crouch and velocity.length() != 0:
		is_sprinted = true
		#camera.fov = 90
		#run_fov()
		#animation.play("Run")
	else:
		is_sprinted = false
		#camera.fov = 75
		#animation.play("Walk")
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var speed = SPEED
	if is_sprinted:
		speed = SPRINT_SPEED
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)

	move_and_slide()
	
	if Input.is_action_just_pressed("select"):
		var node = self
		node = ray.get_collider()
		print(node)
		
		if get_tree().get_nodes_in_group("spots").has(node):
			node.queue_free()
			WashPlayer.play()
			xp += 1
			print(xp)
			spot_label.text = str(xp) + "        "
			
	
	if Input.is_action_just_pressed("pause"):
		if !pause:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
			#get_tree().paused = true
			pause = true
		else :
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
			#get_tree().paused = false
			pause = false
