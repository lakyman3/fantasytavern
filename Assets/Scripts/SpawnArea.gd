extends Area3D

@export var MAX_NPCS = 10 
@export var timer_delay = 2
var count_npcs
var npc = preload("res://Scenes/npc.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start(timer_delay)
	count_npcs = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass


func _on_timer_timeout():
	if count_npcs > MAX_NPCS:
		$Timer.start(timer_delay)
		return
	var new_npc = npc.instantiate()
	add_child(new_npc)
	$Timer.start(timer_delay)
	
